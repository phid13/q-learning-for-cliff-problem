####################################################
# task is to develop q learning algorithm that can #
# learn how to navigate the cliff problem          #
####################################################

import numpy as np
import math
import pandas as pd

# define a method to show grids in a nice way
def display(grid):
    if np.shape(grid)[0] > 20:
        grid = grid[5:20,5:20]
    axis = ["0,0", "0,1", "0,2", "0,3", "0,4", "1,0", "1,1", "1,2", "1,3", "1,4", "2,0", "2,1", "2,2", "2,3", "2,4"]
    print(pd.DataFrame(data=grid, columns=axis, index=axis))

# method to show the map with current state of player
# default s is 10 as this is the starting square
# O = player, | = cliff, * = goal,   = nothing
def show_map(s=15):
    # adjust for padding
    s -= 5

    array = [['[ ]', '[ ]', '[ ]', '[ ]', '[ ]'], ['[ ]', '[ ]', '[ ]', '[ ]', '[ ]'], ['[ ]', '[|]', '[|]', '[|]', '[*]']]
    array[(s//5)][(s%5)] = '[O]'
    print(pd.DataFrame(data=array, columns=[0,1,2,3,4], index=[0,1,2]))

def initialise():
    # define cliff as a 5x3 map
    cliff = np.zeros((3, 5), dtype=int)

    # place character as 1, goal as 2, cliff as 3
    cliff[2, 0] = 1
    cliff [2, 4] = 2
    cliff [2, 1:4] = 3

    # create rewards table with same dimensions as q table
    r = np.full((15, 15), math.nan)

    # all possible moves have reward 0
    for i in range(15):
        r[i,i] = 0 # same square
        if i > 4:
            r[i,(i-5)] = 0 # row above
        if i % 5 != 0:
            r[i,(i-1)] = 0 # column to left
        if i < 10:
            r[i,(i+5)] = 0 # row below
        if (i+1) % 5 != 0:
            r[i,(i+1)] = 0 # column to right

    # reward for going off cliff is -100
    r[10,11] = -100 # (2,0) - (2,1)
    r[11,12] = -100 # (2,1) - (2,2)
    r[12,13] = -100 # (2,2) - (2,3)
    r[6,11]  = -100 # (1,1) - (2,1)
    r[7,12]  = -100 # (1,2) - (2,2)
    r[8,13]  = -100 # (1,3) - (2,3)
    r[14,13] = -100 # (2,4) - (2,3)
    r[13,12] = -100 # (2,3) - (2,2)
    r[12,11] = -100 # (2,2) - (2,1)
    r[11,11] = -100 # (2,1) - (2,1)
    r[12,12] = -100 # (2,2) - (2,2)
    r[13,13] = -100 # (2,3) - (2,3)

    # reward for getting to goal is 50
    r[13,14] = 50   # (2,3) - (2,4)
    r[9,14]  = 50   # (1,4) - (2,4)
    r[14,14] = 50   # (2,4) - (2,4)

    # add a border of nans to r table, and zeros to q table, to make life easier
    r = np.pad(r, 5, mode='constant', constant_values=math.nan)

    return r

# alpha is learning rate, gamma is discount factor
def qstep(q, r, s, alpha, gamma):

    # Qnew = Q + alpha * (reward + gamma * max(Qfuture))
    Qsame = q[s,s] + alpha * (r[s,s] + gamma * np.nanmax([q[s,s+1], q[s,s-1], q[s,s+5], q[s,s-5]]) - q[s,s])
    if s < 15:
        Qdown = q[s,s+5] + alpha * (r[s,s+5] + gamma * np.nanmax([q[s+5,s+6], q[s+5,s+4], q[s+5,s+10], q[s+5,s]]) - q[s,s+5])
    else:
        Qdown = math.nan
    if s < 19:
        Qright = q[s,s+1] + alpha * (r[s,s+1] + gamma * np.nanmax([q[s+1,s+2], q[s+1,s], q[s+1,s+6], q[s+1,s-4]]) - q[s,s+1])
    else:
        Qright = math.nan 
    if s > 9:
        Qup = q[s,s-5] + alpha * (r[s,s-5] + gamma * np.nanmax([q[s-5,s-6], q[s-5,s-4], q[s-5,s-10], q[s-5,s]]) - q[s,s-5])
    else:
        Qup = math.nan
    if s > 5:
        Qleft = q[s,s-1] + alpha * (r[s,s-1] + gamma * np.nanmax([q[s-1,s-2], q[s-1,s], q[s-1,s-6], q[s-1,s+4]]) - q[s,s-1])
    else:
        Qleft = math.nan

    values = np.array([Qsame, Qdown, Qright, Qup, Qleft])

    s_new, q = softmax(s, q, values)

    return s_new, q

def greedy(s, q, values):
    choice = np.nanargmax(values)
    s_new = s
    if choice == 1:
        s_new = s + 5
    elif choice == 2:
        s_new = s + 1
    elif choice == 3:
        s_new = s - 5
    elif choice == 4:
        s_new = s - 1
    q_new = q
    q_new[s_new, s] = values[choice]
    return s_new, q_new

def softmax(s, q, values):
    # normalise first

    # this step is not in softmax, but I am using it to get reasonable numbers for exponentials
    # since using values such as -100 and 50 creates such large probabilities that no exploration
    # will take place
    temp_values = values - np.nanmin(values)
    if np.nanmax(temp_values) != 0:
        temp_values /= np.nanmax(temp_values)

    # need e^x for each value, e^math.nan = 0
    exp_values = np.nan_to_num(np.exp(temp_values))
    # get normalised softmax probabilities of each
    probabilities = []
    for val in exp_values:
        probabilities.append(val / np.sum(exp_values))
    indeces = np.arange(5)
    choice = np.random.choice(indeces, p = probabilities)
    s_new = s
    if choice == 1:
        s_new = s + 5
    elif choice == 2:
        s_new = s + 1
    elif choice == 3:
        s_new = s - 5
    elif choice == 4:
        s_new = s - 1
    q_new = q
    q_new[s_new, s] = values[choice]
    return s_new, q_new

def qlearning(r):

    # create empty q table
    # this must contain every possible state on each axis
    # possible states are simply the character being on every square
    # there are 15 squares, so q table will be 15x15
    # order on each axis is (0,0), (0,1) ... (0,4), (1,0) ... (2,4)
    # however, q table also needs padding to mach r table, so 25x25
    q = np.zeros((np.shape(r)))

    # states will be represented by position of character
    # total states is 25, with 25 actions from each state, although at most 5 are actually possible

    # s is the start state on square (2,0) which  is position 11 in the table
    s = 15 

    alpha = 0.5
    gamma = 0.5

    counter = 0
    while counter != 5000:
        s, q = qstep(q, r, s, alpha, gamma)
        counter += 1
    return q


## RESULTS ##

# method to demonstrate path of bug
def show_path(q, s=10):
    show_map(s+5)

    # adjust for padding
    s += 5

    while s != 19:
        val_same = q[s,s]
        if s < 15:
            val_down = q[s,s+5]
        else:
            val_down = math.nan
        if s < 19:
            val_right = q[s,s+1]
        else:
            val_right = math.nan 
        if s > 9:
            val_up = q[s,s-5]
        else:
            val_up = math.nan
        if s > 5:
            val_left = q[s,s-1]
        else:
            val_left = math.nan
        values = np.array([val_same, val_down, val_right, val_up, val_left])

        ## softmax path choosing

        # use a softmax to choose path, but ignore q_new since 
        # q table is no longer being updated
        # s, q_new = softmax(s, q, values)

        ## greedy path choosing (probably better)
        s, q_new = greedy(s, q, values)

        show_map(s)

# main method for console ui that the user can interact with
def Main():
    r = initialise()
    q_table = qlearning(r)
    x = ' '
    while x != '5':
        print("Welcome to the cliff problem")
        print("1 - view map")
        print("2 - view rewards table")
        print("3 - view q-table")
        print("4 - view path across map")
        print("5 - exit")
        x = input()
        if x == '1':
            show_map()
            y = input()
        if x == '2':
            display(r)
            y = input()
        if x == '3':
            display(q_table)
            y = input()
        if x == '4':
            print("Enter the coordinates of the starting square")
            print("Required format is (0-2, 0-4), e.g. '2, 0'")
            coordinates = input()
            try:
                sx = int(coordinates[0])
                sy = int(coordinates[3])
                s = 5 * sx + sy
                if (s > 14) | (s < 0):
                    print("invalid coordinates format")
                else:
                    show_path(q_table, s=s)
            except:
                print("except")
                print("invalid coordinates format")
            y = input()

Main()
